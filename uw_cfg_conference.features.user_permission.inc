<?php

/**
 * @file
 * uw_cfg_conference.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function uw_cfg_conference_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'adminster uw_conference_admin settings'.
  $permissions['adminster uw_conference_admin settings'] = array(
    'name' => 'adminster uw_conference_admin settings',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'uw_conference_admin',
  );

  // Exported permission: 'conference meta-data'.
  $permissions['conference meta-data'] = array(
    'name' => 'conference meta-data',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'uw_restful_conference',
  );

  // Exported permission: 'create conference_advertisement content'.
  $permissions['create conference_advertisement content'] = array(
    'name' => 'create conference_advertisement content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'create conference_links content'.
  $permissions['create conference_links content'] = array(
    'name' => 'create conference_links content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'create conference_session content'.
  $permissions['create conference_session content'] = array(
    'name' => 'create conference_session content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'create conference_speaker content'.
  $permissions['create conference_speaker content'] = array(
    'name' => 'create conference_speaker content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'create conference_sponsor content'.
  $permissions['create conference_sponsor content'] = array(
    'name' => 'create conference_sponsor content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'create conference_video content'.
  $permissions['create conference_video content'] = array(
    'name' => 'create conference_video content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete any conference_advertisement content'.
  $permissions['delete any conference_advertisement content'] = array(
    'name' => 'delete any conference_advertisement content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete any conference_links content'.
  $permissions['delete any conference_links content'] = array(
    'name' => 'delete any conference_links content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete any conference_session content'.
  $permissions['delete any conference_session content'] = array(
    'name' => 'delete any conference_session content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete any conference_speaker content'.
  $permissions['delete any conference_speaker content'] = array(
    'name' => 'delete any conference_speaker content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete any conference_sponsor content'.
  $permissions['delete any conference_sponsor content'] = array(
    'name' => 'delete any conference_sponsor content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete any conference_video content'.
  $permissions['delete any conference_video content'] = array(
    'name' => 'delete any conference_video content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete own conference_advertisement content'.
  $permissions['delete own conference_advertisement content'] = array(
    'name' => 'delete own conference_advertisement content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete own conference_links content'.
  $permissions['delete own conference_links content'] = array(
    'name' => 'delete own conference_links content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete own conference_session content'.
  $permissions['delete own conference_session content'] = array(
    'name' => 'delete own conference_session content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete own conference_speaker content'.
  $permissions['delete own conference_speaker content'] = array(
    'name' => 'delete own conference_speaker content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete own conference_sponsor content'.
  $permissions['delete own conference_sponsor content'] = array(
    'name' => 'delete own conference_sponsor content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete own conference_video content'.
  $permissions['delete own conference_video content'] = array(
    'name' => 'delete own conference_video content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete terms in conference_blog_categories'.
  $permissions['delete terms in conference_blog_categories'] = array(
    'name' => 'delete terms in conference_blog_categories',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'taxonomy',
  );

  // Exported permission: 'delete terms in conference_session_topics'.
  $permissions['delete terms in conference_session_topics'] = array(
    'name' => 'delete terms in conference_session_topics',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'taxonomy',
  );

  // Exported permission: 'delete terms in conference_session_types'.
  $permissions['delete terms in conference_session_types'] = array(
    'name' => 'delete terms in conference_session_types',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'taxonomy',
  );

  // Exported permission: 'delete terms in conference_speaker_groups'.
  $permissions['delete terms in conference_speaker_groups'] = array(
    'name' => 'delete terms in conference_speaker_groups',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'taxonomy',
  );

  // Exported permission: 'delete terms in conference_sponsorship_levels'.
  $permissions['delete terms in conference_sponsorship_levels'] = array(
    'name' => 'delete terms in conference_sponsorship_levels',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'taxonomy',
  );

  // Exported permission: 'edit any conference_advertisement content'.
  $permissions['edit any conference_advertisement content'] = array(
    'name' => 'edit any conference_advertisement content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit any conference_links content'.
  $permissions['edit any conference_links content'] = array(
    'name' => 'edit any conference_links content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit any conference_session content'.
  $permissions['edit any conference_session content'] = array(
    'name' => 'edit any conference_session content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit any conference_speaker content'.
  $permissions['edit any conference_speaker content'] = array(
    'name' => 'edit any conference_speaker content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit any conference_sponsor content'.
  $permissions['edit any conference_sponsor content'] = array(
    'name' => 'edit any conference_sponsor content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit any conference_video content'.
  $permissions['edit any conference_video content'] = array(
    'name' => 'edit any conference_video content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit own conference_advertisement content'.
  $permissions['edit own conference_advertisement content'] = array(
    'name' => 'edit own conference_advertisement content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit own conference_links content'.
  $permissions['edit own conference_links content'] = array(
    'name' => 'edit own conference_links content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit own conference_session content'.
  $permissions['edit own conference_session content'] = array(
    'name' => 'edit own conference_session content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit own conference_speaker content'.
  $permissions['edit own conference_speaker content'] = array(
    'name' => 'edit own conference_speaker content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit own conference_sponsor content'.
  $permissions['edit own conference_sponsor content'] = array(
    'name' => 'edit own conference_sponsor content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit own conference_video content'.
  $permissions['edit own conference_video content'] = array(
    'name' => 'edit own conference_video content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit terms in conference_blog_categories'.
  $permissions['edit terms in conference_blog_categories'] = array(
    'name' => 'edit terms in conference_blog_categories',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'taxonomy',
  );

  // Exported permission: 'edit terms in conference_session_topics'.
  $permissions['edit terms in conference_session_topics'] = array(
    'name' => 'edit terms in conference_session_topics',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'taxonomy',
  );

  // Exported permission: 'edit terms in conference_session_types'.
  $permissions['edit terms in conference_session_types'] = array(
    'name' => 'edit terms in conference_session_types',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'taxonomy',
  );

  // Exported permission: 'edit terms in conference_speaker_groups'.
  $permissions['edit terms in conference_speaker_groups'] = array(
    'name' => 'edit terms in conference_speaker_groups',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'taxonomy',
  );

  // Exported permission: 'edit terms in conference_sponsorship_levels'.
  $permissions['edit terms in conference_sponsorship_levels'] = array(
    'name' => 'edit terms in conference_sponsorship_levels',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'taxonomy',
  );

  // Exported permission: 'enter conference_advertisement revision log entry'.
  $permissions['enter conference_advertisement revision log entry'] = array(
    'name' => 'enter conference_advertisement revision log entry',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'enter conference_session revision log entry'.
  $permissions['enter conference_session revision log entry'] = array(
    'name' => 'enter conference_session revision log entry',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'enter conference_speaker revision log entry'.
  $permissions['enter conference_speaker revision log entry'] = array(
    'name' => 'enter conference_speaker revision log entry',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'enter conference_sponsor revision log entry'.
  $permissions['enter conference_sponsor revision log entry'] = array(
    'name' => 'enter conference_sponsor revision log entry',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'enter conference_video revision log entry'.
  $permissions['enter conference_video revision log entry'] = array(
    'name' => 'enter conference_video revision log entry',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override conference_advertisement authored by option'.
  $permissions['override conference_advertisement authored by option'] = array(
    'name' => 'override conference_advertisement authored by option',
    'roles' => array(),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override conference_advertisement authored on option'.
  $permissions['override conference_advertisement authored on option'] = array(
    'name' => 'override conference_advertisement authored on option',
    'roles' => array(),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override conference_advertisement promote to front page option'.
  $permissions['override conference_advertisement promote to front page option'] = array(
    'name' => 'override conference_advertisement promote to front page option',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override conference_advertisement published option'.
  $permissions['override conference_advertisement published option'] = array(
    'name' => 'override conference_advertisement published option',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override conference_advertisement revision option'.
  $permissions['override conference_advertisement revision option'] = array(
    'name' => 'override conference_advertisement revision option',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override conference_advertisement sticky option'.
  $permissions['override conference_advertisement sticky option'] = array(
    'name' => 'override conference_advertisement sticky option',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override conference_links authored by option'.
  $permissions['override conference_links authored by option'] = array(
    'name' => 'override conference_links authored by option',
    'roles' => array(),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override conference_links authored on option'.
  $permissions['override conference_links authored on option'] = array(
    'name' => 'override conference_links authored on option',
    'roles' => array(),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override conference_links promote to front page option'.
  $permissions['override conference_links promote to front page option'] = array(
    'name' => 'override conference_links promote to front page option',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override conference_links published option'.
  $permissions['override conference_links published option'] = array(
    'name' => 'override conference_links published option',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override conference_links revision option'.
  $permissions['override conference_links revision option'] = array(
    'name' => 'override conference_links revision option',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override conference_links sticky option'.
  $permissions['override conference_links sticky option'] = array(
    'name' => 'override conference_links sticky option',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override conference_session authored by option'.
  $permissions['override conference_session authored by option'] = array(
    'name' => 'override conference_session authored by option',
    'roles' => array(),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override conference_session authored on option'.
  $permissions['override conference_session authored on option'] = array(
    'name' => 'override conference_session authored on option',
    'roles' => array(),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override conference_session promote to front page option'.
  $permissions['override conference_session promote to front page option'] = array(
    'name' => 'override conference_session promote to front page option',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override conference_session published option'.
  $permissions['override conference_session published option'] = array(
    'name' => 'override conference_session published option',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override conference_session revision option'.
  $permissions['override conference_session revision option'] = array(
    'name' => 'override conference_session revision option',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override conference_session sticky option'.
  $permissions['override conference_session sticky option'] = array(
    'name' => 'override conference_session sticky option',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override conference_speaker authored by option'.
  $permissions['override conference_speaker authored by option'] = array(
    'name' => 'override conference_speaker authored by option',
    'roles' => array(),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override conference_speaker authored on option'.
  $permissions['override conference_speaker authored on option'] = array(
    'name' => 'override conference_speaker authored on option',
    'roles' => array(),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override conference_speaker promote to front page option'.
  $permissions['override conference_speaker promote to front page option'] = array(
    'name' => 'override conference_speaker promote to front page option',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override conference_speaker published option'.
  $permissions['override conference_speaker published option'] = array(
    'name' => 'override conference_speaker published option',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override conference_speaker revision option'.
  $permissions['override conference_speaker revision option'] = array(
    'name' => 'override conference_speaker revision option',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override conference_speaker sticky option'.
  $permissions['override conference_speaker sticky option'] = array(
    'name' => 'override conference_speaker sticky option',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override conference_sponsor authored by option'.
  $permissions['override conference_sponsor authored by option'] = array(
    'name' => 'override conference_sponsor authored by option',
    'roles' => array(),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override conference_sponsor authored on option'.
  $permissions['override conference_sponsor authored on option'] = array(
    'name' => 'override conference_sponsor authored on option',
    'roles' => array(),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override conference_sponsor promote to front page option'.
  $permissions['override conference_sponsor promote to front page option'] = array(
    'name' => 'override conference_sponsor promote to front page option',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override conference_sponsor published option'.
  $permissions['override conference_sponsor published option'] = array(
    'name' => 'override conference_sponsor published option',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override conference_sponsor revision option'.
  $permissions['override conference_sponsor revision option'] = array(
    'name' => 'override conference_sponsor revision option',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override conference_sponsor sticky option'.
  $permissions['override conference_sponsor sticky option'] = array(
    'name' => 'override conference_sponsor sticky option',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override conference_video authored by option'.
  $permissions['override conference_video authored by option'] = array(
    'name' => 'override conference_video authored by option',
    'roles' => array(),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override conference_video authored on option'.
  $permissions['override conference_video authored on option'] = array(
    'name' => 'override conference_video authored on option',
    'roles' => array(),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override conference_video promote to front page option'.
  $permissions['override conference_video promote to front page option'] = array(
    'name' => 'override conference_video promote to front page option',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override conference_video published option'.
  $permissions['override conference_video published option'] = array(
    'name' => 'override conference_video published option',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override conference_video revision option'.
  $permissions['override conference_video revision option'] = array(
    'name' => 'override conference_video revision option',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override conference_video sticky option'.
  $permissions['override conference_video sticky option'] = array(
    'name' => 'override conference_video sticky option',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  return $permissions;
}
