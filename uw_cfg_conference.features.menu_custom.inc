<?php

/**
 * @file
 * uw_cfg_conference.features.menu_custom.inc
 */

/**
 * Implements hook_menu_default_menu_custom().
 */
function uw_cfg_conference_menu_default_menu_custom() {
  $menus = array();

  // Exported menu: menu-conference-links.
  $menus['menu-conference-links'] = array(
    'menu_name' => 'menu-conference-links',
    'title' => 'Conference Links',
    'description' => '',
    'language' => 'und',
    'i18n_mode' => 0,
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('Conference Links');

  return $menus;
}
